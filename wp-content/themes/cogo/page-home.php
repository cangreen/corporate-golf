<!DOCTYPE html>
<html lang="<?php language_attributes() ?>">
<head>
  <meta charset="<?php bloginfo('charset') ?>">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
  <link rel="manifest" href="/manifest.json">
  <title><?php wp_title('«', true, 'right'); ?><?php bloginfo('name'); ?></title>
  <?php wp_head(); ?>
</head>
<body>
<header class="header">
  <div class="header__top">
    <div class="container">
      <div class="header__top-wrapper flex flex-justify-between">
        <div class="header__top-block">
          <p class="header__top-promo">
            <?= ot_get_option('top_subheader') ?>
          </p>
        </div>
        <a href="index.html" class="header__top-block">
          <div class="header__top-brand">
            <p class="header__top-brand header__top-brand_top">
              corporate golf
            </p>
            <p class="header__top-brand header__top-brand_bottom">
              challenge
            </p>
          </div>
        </a>
        <div class="header__top-block flex flex-justify-end">
          <a href="#" class="header__menu-btn flex flex-column flex-justify-between">
            <span></span>
            <span></span>
            <span></span>
          </a>
          <nav class="menu flex">
            <a href="#conception" class="menu__link">Концепция</a>
            <a href="#philosophy" class="menu__link">Философия</a>
            <a href="#calendar" class="menu__link">Календарь</a>
            <a href="#team" class="menu__link">Команда</a>
            <a href="#news" class="menu__link">Новости</a>
            <a href="#partners" class="menu__link">Партнеры</a>
            <a href="#contacts" class="menu__link">Контакты</a>
          </nav>
        </div>
      </div>
    </div>
  </div>
  <div class="header__bottom">
    <div class="container">
      <h1><?= ot_get_option('header') ?></h1>
      <h3><?= ot_get_option('subheader') ?></h3>
      <a href="<?= ot_get_option('header_link') ?>" class="btn btn__header">Подробнее</a>
      <div class="header__icon-wrapper header__icon-wrapper_fixed flex flex-justify-end">
        <a href="#" class="request__btn header__chat-icon"></a>
      </div>
      <div class="header__icon-wrapper flex flex-justify-center">
        <a href="#conception" class="header__mouse-icon"></a>
      </div>
    </div>
  </div>
  <div id="bgndVideo" class="player"
       data-property="{
       videoURL:'<?= ot_get_option('bg_video') ?>',
       containment:'.header',
       autoPlay: true,
       mute: true,
       useOnMobile: false,
       startAt: 0,
       opacity: 1,
	   optimizeDisplay: true,
       showControls: false,
       quality: 'hd720'
       }">
  </div>
  <div class="header__overlay"></div>
</header>
<div class="modal header__modal">
  <div class="modal__bg">
  </div>
  <div class="modal__content">
    <div class="flex flex-justify-end">

      <div class="modal__close"><i class="fas fa-times"></i></div>
    </div>
    <header class="modal__header">Корпоративный гольф</header>
    <div class="modal__body">
      <?= ot_get_option('header_text_full') ?>
    </div>
  </div>
</div>
<section id="conception" class="block concept">
  <div class="container">
    <h2 class="section__subheader">уникальный формат чемпионата</h2>
    <h3 class="section__header">Концепция</h3>
    <div class="slick__container">
      <?php foreach (getFormats() as $format): ?>
        <div class="concept__block lg-3 md-6 sm-12">
          <div class="concept__img"
               style="background-image: url('<?= $format['img'] ?>')">
            <?php if (!empty($format['content'])): ?>
              <a href="#" class="concept__link" data-content="<?= $format['id'] ?>">Подробнее</a>
            <?php endif; ?>
          </div>
          <header class="concept__block-header"><?= $format['title'] ?></header>
          <p class="concept__block-text"><?= $format['subheader'] ?></p>
        </div>
      <?php endforeach; ?>
    </div>
  </div>
</section>
<div class="modal concept__modal">
  <div class="modal__bg">
  </div>
  <div class="modal__content">
    <div class="flex flex-justify-end">

      <div class="modal__close"><i class="fas fa-times"></i></div>
    </div>
    <header class="modal__header"></header>
    <div class="modal__body scroll-pane"></div>
  </div>
</div>
<section class="block block__bg-dark request">
  <div class="container">
    <h2 class="request__header"><?= ot_get_option('final_header') ?></h2>
    <?= ot_get_option('final_text') ?>
    <a href="#" class="btn request__btn">Оставить заявку</a>
  </div>
</section>
<div class="modal request__modal">
  <div class="modal__bg">
  </div>
  <div class="modal__content">
    <div class="flex flex-justify-end">

      <div class="modal__close"><i class="fas fa-times"></i></div>
    </div>
    <header class="modal__header">
      Отправить заявку
    </header>
    <div class="modal__body">
      <p class="modal__text">Заполните заявку с контактными данными и наш специалист свяжется с вами в течение 24
        часов
      </p>
      <?= do_shortcode('[contact-form-7 id="50" class="request__form" title="Contact form"]') ?>
    </div>
  </div>
</div>
<div class="modal request__modal request__modal_success">
  <div class="modal__bg"></div>
  <div class="modal__content">
    <div class="flex flex-justify-end">

      <div class="modal__close"><i class="fas fa-times"></i></div>
    </div>
    <header class="modal__header">
    </header>
    <div class="modal__body">
      <p class="modal__text modal__text_success"><strong>Спасибо, ваша заявка успешно отправлена</strong></p>
      <p class="modal__text modal__text_success">Добро пожаловать в деловой клуб истинных Чемпионов!</p>
    </div>
  </div>
</div>
<section id="philosophy" class="block philosophy">
  <div class="container">
    <h3 class="section__subheader"><?= ot_get_option('philosophy_subheader') ?></h3>
    <h2 class="section__header"><?= ot_get_option('philosophy_header') ?></h2>
    <div class="container">
      <div class="row row_no-paddings">
        <div class="philosophy__block philosophy__block_left lg-8 md-12 sm-12">
          <?= ot_get_option('philosophy_text') ?>
        </div>

        <div class="philosophy__block lg-4 md-12 sm-12">
          <div class="philosophy__img-wrapper">
            <img src="<?= ot_get_option('philosophy_img') ?>" alt="Великий Алексей Евгеньевич" class="philosophy__img">
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<section id="calendar" class="block calendar">
  <div class="container">
    <h3 class="section__subheader"><?= ot_get_option('calendar_subheader') ?></h3>
    <h3 class="section__header"><?= ot_get_option('calendar_header') ?></h3>
  </div>
  <div class="container container_schedule">
    <div class="row">
      <div id="schedule" class="calendar__block lg-5 md-6 sm-12">
      </div>
      <div class="calendar__block calendar__event lg-7 md-6 sm-12">
        <div class="calendar__header-top flex flex-justify-between">
          <p class="calendar__league-name"></p>
          <p class="calendar__event-date"></p>
        </div>
        <div class="calendar__header-bottom">
          <p class="calendar__event-name"></p>
          <p class="calendar__event-location"></p>
        </div>
        <div class="calendar__event-description">

        </div>
        <div class="calendar__buttons-group flex">
          <button class="btn calendar__info-btn ">Подробнее</button>
          <a href="https://cogo.timepad.ru/event/986465/" target="__blank" class="btn btn_gold">Купить билет</a>
        </div>
      </div>
    </div>
  </div>
  </div>
</section>
<div class="modal calendar__modal">
  <div class="modal__bg">
  </div>
  <div class="modal__content">
    <div class="flex flex-justify-end">

      <div class="modal__close"><i class="fas fa-times"></i></div>
    </div>
    <header class="modal__header"></header>
    <div class="modal__body">
    </div>
  </div>

</div>
<section id="team" class="block team">
  <h3 class="section__subheader"><?= ot_get_option('team_subheader') ?></h3>
  <h2 class="section__header"><?= ot_get_option('team_header') ?></h2>
  <p class="team__promo"> <?= ot_get_option('team_promo') ?></p>
  <div class="container">
    <div class="team__members slick__container">
      <?php foreach (getTeam() as $member): ?>
        <div class="team__block lg-3 md-6 sm-12">
          <div class="team__img concept__img"
               style="background-image: url(<?= $member['img'] ?>)">
          </div>
          <header class="team__block-name concept__block-header"><?= $member['title'] ?></header>
          <p class="team__block-text concept__block-text"><?= $member['subheader'] ?></p>
          <p class="team__text">
            <?= $member['content'] ?>
          </p>
        </div>
      <?php endforeach; ?>
    </div>
    <div class="team__request">
      <p class="request__text request__strong">*Запишись на свою тренировку</p>
      <a href="#" class="btn request__btn ">Оставить заявку</a>
    </div>
  </div>
</section>
<section id="news" class="block news">
  <h3 class="section__subheader"><?= ot_get_option('news_subheader') ?></h3>
  <h2 class="section__header"><?= ot_get_option('news_header') ?></h2>
  <div class="block__bg-dark">
    <div class="container news__container">
      <header class="news__block-header">социальные медиа</header>
      <div class="row">
        <div class="news__block lg-9 md-12">
          <div class="news__yt-video">
            <?= do_shortcode('[yotuwp type="channel" id="UCUhR3Dkf7W4iWBJ43WTlf-g" ]') ?>
          </div>
        </div>
        <div class="news__block lg-3 md-12">
          <div class="row row_no-paddings">
            <?= do_shortcode('[instagram-feed showfollow=false showbutton=false]') ?>
          </div>
        </div>
      </div>
      <div class="news__social-block flex flex-justify-center">
        <a href="http://www.youtube.com/channel/UCUhR3Dkf7W4iWBJ43WTlf-g" target="_blank"
           class="news__social-icon news__social-icon_yt"><i class="fab fa-youtube"></i></a>
        <a href="http://www.facebook.com/CorporateGolfChalleng" target="_blank" class="news__social-icon news__social-icon_fb"><i
              class="fab fa-facebook-f"></i> </a>
        <a href="http://www.instagram.com/corporate.golf" target="_blank" class="news__social-icon news__social-icon_ig"><i
              class="fab fa-instagram"></i></a>
      </div>
    </div>
  </div>
</section>
<section id="partners" class="block partners">
  <h3 class="section__subheader"><?= ot_get_option('partners_subheader') ?></h3>
  <h2 class="section__header"><?= ot_get_option('partners_header') ?></h2>
  <div class="container">
    <div class="slick__container">
      <?php foreach (getPartners() as $partner): ?>
        <div class="team__block lg-3 md-6 sm-12">
          <div class="team__img concept__img"
               style="background-image: url(<?= $partner['logo'] ?>">
          </div>
          <header class="team__block-name concept__block-header partners__block-header"><?= $partner['title'] ?>
          </header>
          <p class="team__block-text concept__block-text"><?= $partner['content'] ?></p>
        </div>
      <?php endforeach; ?>
    </div>
  </div>
</section>
<section id="contacts" class="contacts block block__bg-dark">
  <h2 class="section__header">контакты</h2>
  <div class="contacts__container">
    <div class="row row_no-paddings">
      <?php foreach (getContacts() as $contact): ?>
        <div class="contacts__block lg-6 md-6 sm-12">
          <header class="contacts__block-header"><?= $contact['title'] ?></header>
          <div class="contacts__block-text">
            <?= $contact['content'] ?>
          </div>
        </div>
      <?php endforeach; ?>
    </div>
  </div>
</section>
<footer class="footer">
  <div class="container">
    <div class="row row_no-paddings">
      <div class="footer__block lg-6 md-6 sm-12">© 2019, ООО «Корпоративный гольф»</div>
      <div class="footer__block lg-6 md-6 sm-12">ОГРН 1197746259919 | ИНН 9709047943
      </div>
    </div>
  </div>
</footer>
<script>
  $(document).ready(function () {
    $(".player").mb_YTPlayer();
  });
</script>
<script>
  $('.slick__container').slick({
    infinite: true,
    slidesToShow: 4,
    slidesToScroll: 1,
    prevArrow: '<a href="#" class="team__nav-icon team__nav-icon_prev"><i class="fas fa-chevron-left"></i></a>',
    nextArrow: '<a href="#" class="team__nav-icon team__nav-icon_next"><i class="fas fa-chevron-right"></i></a>',
    responsive: [
      {
        breakpoint: 992,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 1,
          infinite: true,
        }
      },
      {
        breakpoint: 640,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
  });
</script>
<?php wp_footer() ?>
</body>
</html>
