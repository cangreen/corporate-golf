<?php

define('JS_DIR', get_template_directory_uri() . '/js');
define('VENDOR_DIR', get_template_directory_uri() . '/vendor');

wp_enqueue_script('wp-api');

function styles()
{
  wp_enqueue_style('style', get_stylesheet_uri(), [], '108', 'all');
  wp_register_style('scroll-pane', VENDOR_DIR . '/jsscrollpane/jquery.jscrollpane.css', false, '1.1', 'all');
  wp_register_style('calendar', VENDOR_DIR . '/fullcalendar/core/main.min.css', false, '1.1', 'all');
  wp_register_style('calendar-daygrid', VENDOR_DIR . '/fullcalendar/daygrid/main.min.css', false, '1.1', 'all');
  wp_register_style('slick-slider', VENDOR_DIR . '/slick/slick.css', false, '1.1', 'all');
  wp_register_style('fa', VENDOR_DIR . '/fa/all.min.css', false, '1.1', 'all');
  wp_enqueue_style('scroll-pane');
  wp_enqueue_style('calendar');
  wp_enqueue_style('calendar-daygrid');
  wp_enqueue_style('slick-slider');
  wp_enqueue_style('fa');
}

add_action('wp_enqueue_scripts', 'styles');

function scripts()
{
  wp_deregister_script('jquery');
  wp_register_script('jquery', VENDOR_DIR . '/jquery/jquery.min.js');
  wp_register_script('scroll-pane-js', VENDOR_DIR . '/jsscrollpane/jquery.jscrollpane.min.js');
  wp_register_script('main', JS_DIR . '/script.js', [], '108', true);
  wp_register_script('calendar-js', VENDOR_DIR . '/fullcalendar/core/main.min.js');
  wp_register_script('calendar-daygrid-js', VENDOR_DIR . '/fullcalendar/daygrid/main.min.js');
  wp_register_script('calendar-interaction-js', VENDOR_DIR . '/fullcalendar/interaction/main.min.js');
  wp_register_script('calendar-moment-js', VENDOR_DIR . '/fullcalendar/moment/main.min.js');
  wp_register_script('mousewheel', VENDOR_DIR . '/jsscrollpane/jquery.mousewheel.js');
  wp_register_script('slick-slider-js', VENDOR_DIR . '/slick/slick.min.js');
  wp_register_script('yt-player', VENDOR_DIR . '/ytplayer/jquery.mb.YTPlayer.min.js');

  wp_enqueue_script('jquery');
  wp_enqueue_script('scroll-pane-js');
  wp_enqueue_script('main');
  wp_enqueue_script('calendar-js');
  wp_enqueue_script('calendar-daygrid-js');
  wp_enqueue_script('calendar-interaction-js');
  wp_enqueue_script('calendar-moment-js');
  wp_enqueue_script('mousewheel');
  wp_enqueue_script('slick-slider-js');
  wp_enqueue_script('yt-player');
}

add_action('wp_enqueue_scripts', 'scripts');


/* Formats custom post type*/
add_action('init', function () {
  register_post_type('formats', [
    'labels' => [
      'name' => 'Форматы', // основное название для типа записи
      'singular_name' => 'формат', // название для одной записи этого типа
      'add_new' => 'Добавить формат', // для добавления новой записи
      'add_new_item' => 'Добавление формата', // заголовка у вновь создаваемой записи в админ-панели.
      'edit_item' => 'Редактирование формата', // для редактирования типа записи
      'new_item' => 'Новый формат', // текст новой записи
      'view_item' => 'Смотреть формат', // для просмотра записи этого типа.
      'search_items' => 'Искать формат', // для поиска по этим типам записи
      'not_found' => 'Не найдено', // если в результате поиска ничего не было найдено
      'not_found_in_trash' => 'Не найдено в корзине', // если не было найдено в корзине
    ],
    'public' => true,
    'show_ui' => true, // зависит от public
    'show_in_menu' => true, // показывать ли в меню адмнки
    'show_in_rest' => true, // добавить в REST API. C WP 4.7
    'rest_base' => 'formats', // $post_type. C WP 4.7
    'menu_position' => null,
    'menu_icon' => 'dashicons-performance',
    'hierarchical' => false,
    'supports' => array('title', 'editor', 'page-attributes'), // 'title','editor','author','thumbnail','excerpt','trackbacks','custom-fields','comments','revisions','page-attributes','post-formats'
    'taxonomies' => array(),
  ]);
});

function getFormats()
{
  $formats = [];
  $items = get_posts([
    'orderby' => 'menu_order',
    'numberposts' => 99,
    'order' => 'ASC',
    'post_type' => 'formats'
  ]);

  foreach ($items as $item) {
    $format = [];
    $format['id'] = $item->ID;
    $format['title'] = $item->post_title;
    $format['content'] = $item->post_content;
    $format['subheader'] = get_fields($item->ID)['subheader'];
    $format['img'] = get_fields($item->ID)['image'];
    $formats[] = $format;
  };
  return $formats;
}

/* Team custom post type*/

add_action('init', function () {
  register_post_type('team', [
    'labels' => [
      'name' => 'Тренеры', // основное название для типа записи
      'singular_name' => 'Тренер', // название для одной записи этого типа
      'add_new' => 'Добавить тренера', // для добавления новой записи
      'add_new_item' => 'Добавление тренера', // заголовка у вновь создаваемой записи в админ-панели.
      'edit_item' => 'Редактирование тренера', // для редактирования типа записи
      'new_item' => 'Новый тренер', // текст новой записи
      'view_item' => 'Смотреть тренера', // для просмотра записи этого типа.
      'search_items' => 'Искать тренера', // для поиска по этим типам записи
      'not_found' => 'Не найдено', // если в результате поиска ничего не было найдено
      'not_found_in_trash' => 'Не найдено в корзине', // если не было найдено в корзине
    ],
    'public' => true,
    'show_ui' => true, // зависит от public
    'show_in_menu' => true, // показывать ли в меню адмнки
    'show_in_rest' => true, // добавить в REST API. C WP 4.7
    'rest_base' => 'team', // $post_type. C WP 4.7
    'menu_position' => null,
    'menu_icon' => 'dashicons-welcome-learn-more',
    'hierarchical' => false,
    'supports' => array('title','editor','page-attributes'),
    'taxonomies' => array(),
  ]);
});

function getTeam()
{
  $team_members = [];
  $items = get_posts([
	  'numberposts' => 999,
    'orderby' => 'menu_order',
    'order' => 'ASC',
    'post_type' => 'team'
  ]);


  foreach ($items as $item) {
    $team_member = [];
    $team_member['id'] = $item->ID;
    $team_member['title'] = $item->post_title;
    $team_member['content'] = $item->post_content;
    $team_member['subheader'] = get_fields($item->ID)['subheader'];
    $team_member['img'] = get_fields($item->ID)['image'];
    $team_members[] = $team_member;
  };
  return $team_members;
}

/* Event custom post type*/

add_action('init', function () {
  register_post_type('event', [
    'labels' => [
      'name' => 'События', // основное название для типа записи
      'singular_name' => 'Событие', // название для одной записи этого типа
      'add_new' => 'Добавить событие', // для добавления новой записи
      'add_new_item' => 'Добавление событие', // заголовка у вновь создаваемой записи в админ-панели.
      'edit_item' => 'Редактирование события', // для редактирования типа записи
      'new_item' => 'Новое событие', // текст новой записи
      'view_item' => 'Смотреть событие', // для просмотра записи этого типа.
      'search_items' => 'Искать событие', // для поиска по этим типам записи
      'not_found' => 'Не найдено', // если в результате поиска ничего не было найдено
      'not_found_in_trash' => 'Не найдено в корзине', // если не было найдено в корзине
    ],
    'public' => true,
    'show_ui' => true, // зависит от public
    'show_in_menu' => true, // показывать ли в меню адмнки
    'show_in_rest' => true, // добавить в REST API. C WP 4.7
    'rest_base' => 'event', // $post_type. C WP 4.7
    'menu_position' => null,
    'menu_icon' => 'dashicons-calendar-alt',
    'hierarchical' => false,
    'supports' => array('title', 'editor'), // 'title','editor','author','thumbnail','excerpt','trackbacks','custom-fields','comments','revisions','page-attributes','post-formats'
    'taxonomies' => array(),
  ]);
});

// Add events custom fields data to WP api
add_action('rest_api_init', 'create_event_meta_field');

function create_event_meta_field() {
  register_rest_field( 'event', 'meta', array(
      'get_callback'    => 'get_event_meta_for_api',
      'schema'          => null,
    )
  );
}

function get_event_meta_for_api( $object ) {
  $post_id = $object['id'];
  return get_post_meta( $post_id );
}

/* Partners custom post type*/
add_action('init', function () {
  register_post_type('partners', [
    'labels' => [
      'name' => 'Партнеры', // основное название для типа записи
      'singular_name' => 'Партнер', // название для одной записи этого типа
      'add_new' => 'Добавить партнера', // для добавления новой записи
      'add_new_item' => 'Добавление партнера', // заголовка у вновь создаваемой записи в админ-панели.
      'edit_item' => 'Редактирование партнера', // для редактирования типа записи
      'new_item' => 'Новый партнер', // текст новой записи
      'view_item' => 'Смотреть партнера', // для просмотра записи этого типа.
      'search_items' => 'Искать партнера', // для поиска по этим типам записи
      'not_found' => 'Не найдено', // если в результате поиска ничего не было найдено
      'not_found_in_trash' => 'Не найдено в корзине', // если не было найдено в корзине
    ],
    'public' => true,
    'show_ui' => true, // зависит от public
    'show_in_menu' => true, // показывать ли в меню адмнки
    'show_in_rest' => true, // добавить в REST API. C WP 4.7
    'rest_base' => 'partners', // $post_type. C WP 4.7
    'menu_position' => null,
    'menu_icon' => 'dashicons-buddicons-buddypress-logo',
    'hierarchical' => false,
    'supports' => array('title', 'editor', 'page-attributes'), // 'title','editor','author','thumbnail','excerpt','trackbacks','custom-fields','comments','revisions','page-attributes','post-formats'
    'taxonomies' => array(),
  ]);
});

function getPartners()
{
  $partners = [];
  $items = get_posts([
	 'numberposts' => 999,
    'orderby' => 'menu_order',
    'order' => 'ASC',
    'post_type' => 'partners'
  ]);

  foreach ($items as $item) {
    $partner = [];
    $partner['id'] = $item->ID;
    $partner['title'] = $item->post_title;
    $partner['content'] = $item->post_content;
    $partner['logo'] = get_fields($item->ID)['logo'];
    $partners[] = $partner;
  };
  return $partners;
}

/* Contacts custom post type*/
add_action('init', function () {
  register_post_type('contacts', [
    'labels' => [
      'name' => 'Контакты', // основное название для типа записи
      'singular_name' => 'Контакт', // название для одной записи этого типа
      'add_new' => 'Добавить Контакт', // для добавления новой записи
      'add_new_item' => 'Добавление Контакта', // заголовка у вновь создаваемой записи в админ-панели.
      'edit_item' => 'Редактирование Контакта', // для редактирования типа записи
      'new_item' => 'Новый Контакт', // текст новой записи
      'view_item' => 'Смотреть Контакт', // для просмотра записи этого типа.
      'search_items' => 'Искать Контакт', // для поиска по этим типам записи
      'not_found' => 'Не найдено', // если в результате поиска ничего не было найдено
      'not_found_in_trash' => 'Не найдено в корзине', // если не было найдено в корзине
    ],
    'public' => true,
    'show_ui' => true, // зависит от public
    'show_in_menu' => true, // показывать ли в меню адмнки
    'show_in_rest' => true, // добавить в REST API. C WP 4.7
    'rest_base' => 'contacts', // $post_type. C WP 4.7
    'menu_position' => null,
    'menu_icon' => 'dashicons-id',
    'hierarchical' => false,
    'supports' => array('title', 'editor', 'page-attributes'), // 'title','editor','author','thumbnail','excerpt','trackbacks','custom-fields','comments','revisions','page-attributes','post-formats'
    'taxonomies' => array(),
  ]);
});

function getContacts()
{
  $contacts = [];
  $items = get_posts([
	'numberposts' => 999,
    'orderby' => 'menu_order',
    'order' => 'ASC',
    'post_type' => 'contacts'
  ]);

  foreach ($items as $item) {
    $contact = [];
    $contact['id'] = $item->ID;
    $contact['title'] = $item->post_title;
    $contact['content'] = $item->post_content;
    $contact['logo'] = get_fields($item->ID)['logo'];
    $contacts[] = $contact;
  };
  return $contacts;
}