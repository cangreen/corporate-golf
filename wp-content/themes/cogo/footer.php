<footer class="footer">
  <div class="container">
    <div class="row row_no-paddings">
      <div class="footer__block lg-6 md-6 sm-12">© 2019, COGO</div>
      <div class="footer__block lg-6 md-6 sm-12"><a href="mailto:info@corporate.golf">info@corporate.golf</a> | +7 495
        220-20-74
      </div>
    </div>
  </div>
</footer>
<script>
    $(document).ready(function () {
        $(".player").mb_YTPlayer();
    });
</script>
<script>
    async function getTPevents(){
        let response = await fetch('https://api.timepad.ru/v1/events.json?organization_ids=166484&fields=id,name,description_html,location');
        return await response.json();
    }

    getTPevents().then(data =>{
        const calendarEl = document.getElementById('schedule');
        let events = [];
        for(TPevent of data.values) {
            events.push({
                title: TPevent.name,
                start: TPevent.starts_at,
                content: {
                    id: TPevent.id,
                    desc: TPevent.description_html,
                    loc: TPevent.location
                },
                rendering: 'background',
                allDay: true
            });
        }
        const eventNameEl = document.querySelector('.calendar__event-name');
        const eventLocationEl = document.querySelector('.calendar__event-location');
        const eventDescriptionEl = document.querySelector('.calendar__event-description');
        const calendar = new FullCalendar.Calendar(calendarEl, {
            plugins: ['dayGrid', 'interaction'],
            locale: 'ruLocale',
            contentHeight: 'auto',
            firstDay: 1,
            fixedWeekCount: false,
            defaultDate: events[0].start,
            header: {
                left: 'prev',
                center: 'title',
                right: 'next'
            },
            events: events,
            dateClick: function(info) {
                if(info.jsEvent.target.classList.contains('fc-bgevent'));
                calendar.getEvents().forEach(calEvent => {
                    if(calEvent.start.toDateString() === info.date.toDateString()) {
                        eventNameEl.innerHTML = calEvent.title;
                        eventDescriptionEl.innerHTML = calEvent.extendedProps.content.desc;
                        eventLocationEl.innerHTML = `${calEvent.extendedProps.content.loc.city}, ${calEvent.extendedProps.content.loc.country}`;
                    }
                });
                info.jsEvent.target.style.backgroundColor = '#202131'
            }


        });

        calendar.render();
    });
</script>
<script>
    $('.team__members').slick({
        infinite: true,
        slidesToShow: 4,
        slidesToScroll: 1,
        prevArrow: '<a href="#" class="team__nav-icon team__nav-icon_prev"><i class="fas fa-chevron-left"></i></a>',
        nextArrow: '<a href="#" class="team__nav-icon team__nav-icon_next"><i class="fas fa-chevron-right"></i></a>',
        responsive: [
            {
                breakpoint: 992,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1,
                    infinite: true,
                }
            },
            {
                breakpoint: 640,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    });
</script>
</body>
</html>