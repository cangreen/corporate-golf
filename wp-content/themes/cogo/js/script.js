"use strict"
let jsp_api = $('.scroll-pane').jScrollPane();
$('.header__modal .modal__body').jScrollPane();

/* Mobile menu */
const menu = document.querySelector('.menu');
const menuBtn = document.querySelector('.header__menu-btn');

menuBtn.addEventListener('click', e => {
  e.preventDefault();
  menuBtn.classList.toggle('header__menu-btn_open');
  menu.classList.toggle('menu-open');
});


/* Disable video overlay for mobile devices*/
if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|BB|PlayBook|IEMobile|Windows Phone|Kindle|Silk|Opera Mini/i.test(navigator.userAgent)) {
  document.querySelector('.header__overlay').style = 'display: none';
  document.querySelector('.header').classList.add('header_solid');
}


/* Gallery slide arrows fix*/
const height = document.querySelector('.team__img').clientHeight;
const buttons = document.querySelectorAll('.team__nav-icon');
buttons.forEach(el => {
  el.style.top = `${height / 2 - el.clientHeight / 2}px`;
});

/* JQuery smooth scrolling */

$('a[href*="#"]')
// Remove links that don't actually link to anything
  .not('[href="#"]')
  .not('[href="#0"]')
  .click(function (event) {
    // On-page links
    if (
      location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '')
      &&
      location.hostname == this.hostname
    ) {
      // Figure out element to scroll to
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
      // Does a scroll target exist?
      if (target.length) {
        // Only prevent default if animation is actually gonna happen
        event.preventDefault();
        $('html, body').animate({
          scrollTop: target.offset().top
        }, 1000, function () {
          // Callback after animation
          // Must change focus!
          var $target = $(target);
          $target.focus();
          if ($target.is(":focus")) { // Checking if the target was focused
            return false;
          } else {
            $target.attr('tabindex', '-1'); // Adding tabindex for elements not focusable
            $target.focus(); // Set focus again
          }
        });
      }
    }
  });

/* Concept modal */
const block = document.querySelector('.concept');
const modal = document.querySelector('.concept__modal');
const modalHeader = modal.querySelector('.modal__header');
const closeButton = modal.querySelector('.modal__close');
const content = modal.querySelector('.modal__content');
const modalBg = modal.querySelector('.modal__bg');

block.addEventListener('click', event => {
  if (event.target.className === 'concept__link') {
    event.preventDefault();
    const jsp_data = jsp_api.data('jsp');
    getModalContent(event.target.dataset.content).then(data => {
      modalHeader.innerText = data.title.rendered;
      jsp_data.getContentPane().html('');
      jsp_data.getContentPane().append(data.content.rendered, jsp_data.reinitialise());
      setTimeout(function () {
        jsp_data.getContentPane().append('', jsp_data.reinitialise())
      }, 4);
    });
    modal.classList.add('modal_open');
    content.classList.add('animated');
    content.classList.add('bounceInDown');
    content.classList.add('fast');
    document.documentElement.style.overflowY = 'hidden';
    if (modalBg.clientHeight < content.clientHeight) {
      modalBg.style.height = `${content.clientHeight + 28}px`;
    }
  }
  modal.addEventListener('click', event => {
    event.stopPropagation();
    if (event.target.parentElement.className === 'modal__close' || event.target.className === 'modal__bg')
      modal.classList.remove('modal_open');
    content.classList.remove('animated');
    content.classList.remove('bounceInDown');
    content.classList.remove('fast');
    document.documentElement.style.overflowY = 'auto';
  });
});

async function getModalContent(id) {
  let response = await fetch(`${wpApiSettings.root}wp/v2/formats/${id}`);
  return await response.json();
}

/* Request modal */

const blocks = document.querySelectorAll('.request__btn');
const requestModal = document.querySelector('.request__modal');
const requestSuccessModal = document.querySelector('.request__modal_success');
const requestCloseButton = document.querySelector('.request__modal .modal__close');
const requestContent = requestModal.querySelector('.modal__content');
const requestModalBg = requestModal.querySelector('.modal__bg');
const form = document.querySelector('.wpcf7');
blocks.forEach(block => {
  block.addEventListener('click', event => {
    if (event.target.tagName === 'A') {
      event.preventDefault();
      $('.request__modal .modal__content').wrap("<div class='modal__wrapper'></div>");
      $('.modal__wrapper').jScrollPane({autoReinitialise: true});
      requestModal.classList.add('modal_open');
      requestContent.classList.add('animated');
      requestContent.classList.add('bounceInDown');
      requestContent.classList.add('fast');
      document.documentElement.style.overflowY = 'hidden';
      if (requestModalBg.clientHeight < requestContent.clientHeight) {
        requestModalBg.style.height = `${requestContent.clientHeight + 28}px`;
      }
    }
  })
});
requestModal.addEventListener('click', event => {
  if (event.target.parentElement.className === 'modal__close' || event.target.parentElement.parentElement.className === 'modal__wrapper')
    requestModal.classList.remove('modal_open');
  requestContent.classList.remove('animated');
  requestContent.classList.remove('bounceInDown');
  requestContent.classList.remove('fast');
  document.documentElement.style.overflowY = 'auto';
});

form.addEventListener('wpcf7submit', event => {
  if (event.detail.status === 'mail_sent') {
    requestModal.classList.remove('modal_open');
    requestSuccessModal.classList.add('modal_open');
    requestSuccessModal.classList.add('animated');
    requestSuccessModal.classList.add('fadeIn');
    requestSuccessModal.classList.add('fast');
  }
});

requestSuccessModal.querySelector('.modal__close').addEventListener('click', () => {
  requestSuccessModal.classList.remove('modal_open');
  requestSuccessModal.classList.remove('animated');
  requestSuccessModal.classList.remove('bounceInDown');
  requestSuccessModal.classList.remove('fast');
  document.body.style.overflowY = 'auto';
});

/* Calendar modal */
initModal('.calendar__modal', '.calendar__info-btn');

/* Header modal */
initModal('.header__modal', '.btn__header');

function initModal(modalSelector, openSelector) {
  const btn = document.querySelector(openSelector);
  btn.addEventListener('click', event => {
    event.preventDefault();
    const modal = document.querySelector(modalSelector);
    const content = modal.querySelector('.modal__content');
    const closeBtn = modal.querySelector('.modal__close');
    const bg = modal.querySelector('.modal__bg');

    modal.classList.add('modal_open');
    content.classList.add('animated');
    content.classList.add('bounceInDown');
    content.classList.add('fast');
    document.documentElement.style.overflowY = 'hidden';
    if (bg.clientHeight < content.clientHeight) {
      bg.style.height = `${content.clientHeight + 28}px`;
    }
    modal.addEventListener('click', event => {
      event.stopPropagation();
      if (event.target.parentElement.className === 'modal__close' || event.target.className === 'modal__bg')
        modal.classList.remove('modal_open');
        content.classList.remove('animated');
        content.classList.remove('bounceInDown');
        content.classList.remove('fast');
        document.documentElement.style.overflowY = 'auto';
    });
  });
}

/* Form custom select */

$('select').each(function () {
  var $this = $(this), numberOfOptions = $(this).children('option').length;
  $this.addClass('select-hidden');
  $this.wrap('<div class="select"></div>');
  $this.after('<div class="select-styled"></div>');

  var $styledSelect = $this.next('div.select-styled');
  $styledSelect.text($this.children('option').eq(0).text());

  var $list = $('<ul />', {
    'class': 'select-options'
  }).insertAfter($styledSelect);
  for (var i = 0; i < numberOfOptions; i++) {
    $('<li />', {
      text: $this.children('option').eq(i).text(),
      rel: $this.children('option').eq(i).val()
    }).appendTo($list);
  }

  var $listItems = $list.children('li');

  $styledSelect.click(function (e) {
    e.stopPropagation();
    $('div.select-styled.active').not(this).each(function () {
      $(this).removeClass('active').next('ul.select-options').hide();
    });
    $(this).toggleClass('active').next('ul.select-options').toggle();
  });

  $listItems.click(function (e) {
    e.stopPropagation();
    $styledSelect.text($(this).text()).removeClass('active');
    $this.val($(this).attr('rel'));
    if ($this[0].id === 'category') {
      if ($this.val() === 'Форматы чемпионата') {
        $('select#format').parent().parent().parent().removeClass('form__group_hidden');
        if (!$('select#coaches').parent().parent().hasClass('form__group_hidden')) {
          $('select#coaches').parent().parent().parent().addClass('form__group_hidden');
        }
        // $('.modal__wrapper').jScrollPane();
      } else if ($this.val() === 'Команда тренеров') {
        $('select#coaches').parent().parent().parent().removeClass('form__group_hidden');
        if (!$('select#format').parent().parent().parent().hasClass('form__group_hidden'))
          $('select#format').parent().parent().parent().addClass('form__group_hidden');
      } else {
        if (!$('select#format').parent().parent().parent().hasClass('form__group_hidden'))
          $('select#format').parent().parent().parent().addClass('form__group_hidden');
        if (!$('select#coaches').parent().parent().parent().hasClass('form__group_hidden'))
          $('select#coaches').parent().parent().parent().addClass('form__group_hidden');
      }
    }

    $list.hide();
  });

  $(document).click(function () {
    $styledSelect.removeClass('active');
    $list.hide();
  });
});

/* Calendar Events */

async function getTPevents() {
  let response = await fetch('https://api.timepad.ru/v1/events.json?organization_ids=166484&fields=id,name,description_short,description_html,location');
  return await response.json();
}

getTPevents().then(data => {
  const calendarEl = document.getElementById('schedule');
  let events = [];
  data.values.forEach(event => {
    events.push({
      title: event.name,
      start: event.starts_at,
      id: event.id,
      content: {
        full_desc: event.description_html,
        short_desc: event.description_short,
        loc: event.location
      },
      rendering: 'background',
      allDay: true
    });
  });
  const eventNameEl = document.querySelector('.calendar__event-name');
  const eventDateEl = document.querySelector('.calendar__event-date');
  const eventLocationEl = document.querySelector('.calendar__event-location');
  const eventDescriptionEl = document.querySelector('.calendar__event-description');
  const calendar = new FullCalendar.Calendar(calendarEl, {
    plugins: ['dayGrid', 'interaction'],
    locale: 'ruLocale',
    contentHeight: 'auto',
    firstDay: 1,
    fixedWeekCount: false,
    defaultDate: events[0].start,
    header: {
      left: 'prev',
      center: 'title',
      right: 'next'
    },
    events: events,
    // dateClick: function(info) {
    //     if(info.jsEvent.target.classList.contains('fc-bgevent'));
    //     calendar.getEvents().forEach(calEvent => {
    //         if(calEvent.start.toDateString() === info.date.toDateString()) {
    //             eventNameEl.innerHTML = calEvent.title;
    //             eventDescriptionEl.innerHTML = calEvent.extendedProps.content.desc;
    //             eventLocationEl.innerHTML = `${calEvent.extendedProps.content.loc.city}, ${calEvent.extendedProps.content.loc.country}`;
    //         }
    //     });
    //     info.jsEvent.target.style.backgroundColor = '#202131'
    // }


  });

  calendar.render();
  calendar.getEvents().forEach(calEvent => {
    eventNameEl.innerHTML = calEvent.title;
    eventDateEl.innerHTML = calendar.formatDate(calEvent.start, {
      month: 'numeric',
      year: 'numeric',
      day: 'numeric',

    });
    eventDescriptionEl.innerHTML = calEvent.extendedProps.content.short_desc;
    eventLocationEl.innerHTML = `${calEvent.extendedProps.content.loc.city}, ${calEvent.extendedProps.content.loc.country}`;
    fetch(`${wpApiSettings.root}wp/v2/event`).then(response => {
      return response.json()
    }).then(data => {
      data.forEach(apiEvent => {
        const event_id = apiEvent.meta.timepad_event_id[0];
        if (event_id === calEvent.id) {
          const modal = document.querySelector('.calendar__modal');
          const modalBody = modal.querySelector('.modal__body');
          const modalHeader = modal.querySelector('.modal__header');
          const leagueName = document.querySelector('.calendar__league-name');
          modalHeader.innerHTML = calEvent.title;
          modalBody.innerHTML = apiEvent.content.rendered;
          leagueName.innerHTML = apiEvent.meta.event_format[0];

          $('.calendar__modal .modal__body').jScrollPane();
        }
      });
    });

  });
});

/* News fixes*/
document.querySelector('.yotu-player').style.setProperty('padding-bottom', '66%', 'important');
window.onload = function () {
  setTimeout(() => {
    document.querySelector('.sbi_type_image').style.setProperty('margin-bottom', '25px', 'important');
  }, 2000);
};













