<!DOCTYPE html>
<html lang="<?php language_attributes()?>">
<head>
  <meta charset="<?php bloginfo('charset')?>">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
  <title><?php wp_title('«', true, 'right'); ?> <?php bloginfo('name'); ?></title>
  <?php wp_head(); ?>
</head>
<body>
<header class="header">
  <div class="header__top">
    <div class="container">
      <div class="header__top-wrapper flex flex-justify-between">
        <div class="header__top-block">
          <p class="header__top-promo">
            Новый тренд для вашего бизнесa
          </p>
        </div>
        <a href="index.html" class="header__top-block">
          <div class="header__top-brand">
            <p class="header__top-brand header__top-brand_top">
              corporate golf
            </p>
            <p class="header__top-brand header__top-brand_bottom">
              challenge
            </p>
          </div>
        </a>
        <div class="header__top-block flex flex-justify-end">
          <a href="#" class="header__menu-btn flex flex-column flex-justify-between">
            <span></span>
            <span></span>
            <span></span>
          </a>
          <nav class="menu flex">
            <a href="#conception" class="menu__link">Концепция</a>
            <a href="#philosophy" class="menu__link">Философия</a>
            <a href="#calendar" class="menu__link">Календарь</a>
            <a href="#team" class="menu__link">Команда</a>
            <a href="#news" class="menu__link">Новости</a>
            <a href="#partners" class="menu__link">Партнеры</a>
            <a href="#contacts" class="menu__link">Контакты</a>
          </nav>
        </div>
      </div>
    </div>
  </div>
  <div class="header__bottom">
    <div class="container">
      <h1>БИЗНЕС ПЛОЩАДКА С ИСТИННОЙ <br>ПРИРОДОЙ ПОБЕДЫ И УСПЕХА</h1>
      <h3>Корпоративный гольф - это такой вид
        спорта, в котором главное и результат, и партнеры.</h3>
      <a href="#philosophy" class="btn btn__header">Подробнее</a>
      <div class="header__icon-wrapper flex flex-justify-end">
        <a href="#" class="header__chat-icon"></a>
      </div>
      <div class="header__icon-wrapper flex flex-justify-center">
        <a href="#conception" class="header__mouse-icon"></a>
      </div>
    </div>
  </div>
  <div id="bgndVideo" class="player"
       data-property="{
       videoURL:'https://youtu.be/PVxpqmE9CUI',
       containment:'.header',
       autoPlay: true,
       mute: true,
       useOnMobile: false,
       startAt: 0,
       opacity: 1,
       showControls: false,
       quality: 'hd720'
       }">
  </div>
  <div class="header__overlay"></div>
</header>